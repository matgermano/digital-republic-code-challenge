# Code Challenge

<div align="center">
<img src="https://cdn.worldvectorlogo.com/logos/golang-gopher.svg" width="100">
<img src="https://media.glassdoor.com/sql/2840415/digital-republic-brazil-squarelogo-1598541323549.png" width="250">
</div>

[![status: experimental](https://github.com/GIScience/badges/raw/master/status/experimental.svg)](https://github.com/GIScience/badges#experimental)


## Rules

1. No walls can be less than 1 square meter or more than 15 square meters, but they can have different heights and widths
2. The total area of doors and windows must be a maximum of 50% of the wall area
3. The height of walls with a door must be at least 30 centimeters higher than the height of the door
4. Each window has the measurements: 2.00 x 1.20 meter
5. Each door has the measurements: 0.80 x 1.90
6. Each liter of paint is capable of painting 5 square meters
7. Do not consider ceiling or floor.
8. The size variations of the paint cans are:

- 0,5 L
- 2,5 L
- 3,6 L
- 18 L

## Important Notice

The rules don't mention anything about leftovers. So, if we have a room that needs 5L of paint to be painted, we can suggest a single 18L bucket.<br>
If leftovers were to be taken into consideration, we should instead suggest 2 2.5L buckets.<br>
As the challenge isn't clear about this, I made a flag `(--leftover)`. If the flag isn't passed, the program will not take leftovers into consideration.<br>
If, on the other hand, the flag is passed, the program will take leftovers into consideration.<br>
For more information, read the comments on the `(r room) PaintBucketsNeeded()` func.

## Requirements
1. [Go](https://go.dev/dl/) 1.18 or above
2. Git
3. Your IDE of choice

## Installation
1. Download and install go
2. Choose your location to add the repository, open your git bash there and run the follow command:
```
git clone https://gitlab.com/matgermano/digital-republic-code-challenge
```

## Instructions (without the leftover flag)
1. Open your IDE, navigate to the digital-republic-code-challenge folder and in the terminal type the following command:
```
go run main.go
```
2. The terminal will return:
```
Enter first wall height
```
Then, in the terminal itself, you will place the measurement, always remembering to respect the stipulated challenge rules<br>

3. In addition to the height of the wall, you will fill in its width and number of doors and windows, just as you filled in the height, for example:
```
Enter first wall height
3
Enter first wall width
2
Enter amount of windows on the first wall
1
Enter amount of doors on the first wall
0
```
4. Now you will just need to fill the exact same fields for all the walls, for example:
```
Enter first wall height
3
Enter first wall width
2
Enter amount of windows on the first wall
1
Enter amount of doors on the first wall
0
Enter second wall height
3
Enter second wall width
2
Enter amount of windows on the second wall
0
Enter amount of doors on the second wall
1
Enter third wall height
3
Enter third wall width
2
Enter amount of windows on the third wall
1
Enter amount of doors on the third wall
0
Enter fourth wall height
3
Enter fourth wall width
3
Enter amount of windows on the fourth wall
0
Enter amount of doors on the fourth wall
0


Your room has 27.0000 sqrmt in total
It has 20.6800 sqrmt of paintable space
It will need 4.1360 liters of paint
You will need 1 18.0L bucket
```

## Instructions (with the leftover flag)
1. Open your IDE, navigate to the digital-republic-code-challenge folder and in the terminal type the following command:
```
go run main.go
```
2. The terminal will return:
```
Enter first wall height
```
Then, in the terminal itself, you will place the measurement, always remembering to respect the stipulated challenge rules<br>

3. In addition to the height of the wall, you will fill in its width and number of doors and windows, just as you filled in the height, for example:
```
Enter first wall height
3
Enter first wall width
2
Enter amount of windows on the first wall
1
Enter amount of doors on the first wall
0
```
4. Now you will just need to fill the exact same fields for all the walls, for example:
```
Enter first wall height
3
Enter first wall width
2
Enter amount of windows on the first wall
1
Enter amount of doors on the first wall
0
Enter second wall height
3
Enter second wall width
2
Enter amount of windows on the second wall
0
Enter amount of doors on the second wall
1
Enter third wall height
3
Enter third wall width
2
Enter amount of windows on the third wall
1
Enter amount of doors on the third wall
0
Enter fourth wall height
3
Enter fourth wall width
3
Enter amount of windows on the fourth wall
0
Enter amount of doors on the fourth wall
0


Your room has 27.0000 sqrmt in total
It has 20.6800 sqrmt of paintable space
It will need 4.1360 liters of paint
You will need 1 3.6L bucket and 2 0.5L buckets
```

## Errors
If you do not follow any of the rules described, the system will return exactly which error you made through a panic. As an example:
```
Enter first wall height
10
Enter first wall width
10
Enter amount of windows on the first wall
2
Enter amount of doors on the first wall
1
panic: wall size can't be smaller than 1 sqrmt nor bigger than 15 sqrmt
```
In this case, you will have to run the terminal again exactly as described above, placing the measurements according to the challenge rules.