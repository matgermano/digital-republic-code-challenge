package painter

import (
	"math"
	"reflect"
	"testing"
)

func TestNewRoom(t *testing.T) {
	FlagConsiderLeftovers = false

	firstWall, err := NewWall(2.2, 4, 0, 0)
	if err != nil {
		t.Fatalf("firstWall failed to build, err: %s", err.Error())
	}

	secondWall, err := NewWall(2.2, 4, 0, 0)
	if err != nil {
		t.Fatalf("secondWall failed to build, err: %s", err.Error())
	}

	thirdWall, err := NewWall(2.2, 4, 1, 0)
	if err != nil {
		t.Fatalf("thirdWall failed to build, err: %s", err.Error())
	}

	fourthWall, err := NewWall(2.2, 4, 0, 1)
	if err != nil {
		t.Fatalf("fourthWall failed to build, err: %s", err.Error())
	}

	// Expected results
	const (
		roomSize            = (2.2 * 4) * 4
		roomPaintableSize   = roomSize - DoorSize - WindowSize
		amountOfPaintNeeded = roomPaintableSize / PaintWorkQuantity
	)

	var (
		expectedBuckets = map[PaintBucket]int{
			LargePaintBucket: 1,
		}
	)

	room, err := NewRoom([4]Wall{firstWall, secondWall, thirdWall, fourthWall})
	if err != nil {
		t.Fatal(err)
	}

	if !almostEqual(room.Size(), roomSize) {
		t.Errorf("room size is wrong. expected %.4f but got %.4f", roomSize, room.Size())
	}

	if !almostEqual(room.PaintableSize(), roomPaintableSize) {
		t.Errorf("room paintable size is wrong. expected %.4f but got %.4f", roomPaintableSize, room.PaintableSize())
	}

	if !almostEqual(room.PaintNeeded(), amountOfPaintNeeded) {
		t.Errorf("room amount of paint needed is wrong. expected %.4f but got %.4f", amountOfPaintNeeded, room.PaintNeeded())
	}

	if !reflect.DeepEqual(room.PaintBucketsNeeded(), expectedBuckets) {
		t.Errorf("paint buckets needed to paint room is wrong. expected %#v but got %#v", room.PaintBucketsNeeded(), expectedBuckets)
	}
}

func TestNewWall(t *testing.T) {
	var (
		wall Wall
		err  error
	)

	wall, err = NewWall(2.2, 3, 0, 0)
	if err != nil {
		t.Fatalf("Wall (2.2, 3, 0, 0) erroed, but should build fine. Err: %s", err.Error())
	}

	if wall.height != 2.2 {
		t.Errorf("Wall (2.2, 3, 0, 0) should have a 2.2 height but instead have a %v height", wall.height)
	}

	if wall.width != 3.0 {
		t.Errorf("Wall (2.2, 3, 0, 0) should have a 3.0 width but instead have a %v width", wall.width)
	}

	if wall.amountOfDoors > 0 {
		t.Errorf("Wall (2.2, 3, 0, 0) should have a 0 doors but instead have a %d doors", wall.amountOfDoors)
	}

	if wall.amountOfWindows > 0 {
		t.Errorf("Wall (2.2, 3, 0, 0) should have a 0 windows but instead have a %d windows", wall.amountOfWindows)
	}

	if wall.Size() != wall.height*wall.width {
		t.Errorf("wall size is wrong. expected %v but got %v", wall.height*wall.width, wall.Size())
	}

	if wall.PaintableSize() != wall.height*wall.width {
		t.Errorf("paintable wall size is wrong. expected %.4f but got %.4f", wall.height*wall.width, wall.PaintableSize())
	}

	if err := wall.Validate(); err != nil {
		t.Errorf("wall is coming out as invalid when it should be valid. error: %s", err.Error())
	}

	_, err = NewWall(2.1, 3, 0, 0)
	if err != nil {
		t.Fatalf("Wall (2.1, 3, 0, 0) erroed, but should build fine. Err: %s", err.Error())
	}

	_, err = NewWall(2.1, 3, 1, 0)
	if err == nil {
		t.Fatalf("Wall (2.1, 3, 1, 0) built fine but shouldn't, because the wall is not high enough to accomodate a door")
	}

	_, err = NewWall(1, 1, 5, 5)
	if err == nil {
		t.Fatal("Wall (1, 1, 5, 5) build fine but shouldn't, because the area of the windows and doors are greater than 50% of the total area")
	}
}

func TestLeftoverLogic(t *testing.T) {
	FlagConsiderLeftovers = true

	firstWall, err := NewWall(2.2, 4, 0, 0)
	if err != nil {
		t.Fatalf("firstWall failed to build, err: %s", err.Error())
	}

	secondWall, err := NewWall(2.2, 4, 0, 0)
	if err != nil {
		t.Fatalf("secondWall failed to build, err: %s", err.Error())
	}

	thirdWall, err := NewWall(2.2, 4, 1, 0)
	if err != nil {
		t.Fatalf("thirdWall failed to build, err: %s", err.Error())
	}

	fourthWall, err := NewWall(2.2, 4, 0, 1)
	if err != nil {
		t.Fatalf("fourthWall failed to build, err: %s", err.Error())
	}

	// Expected results
	var (
		roomSize            = (2.2 * 4) * 4
		roomPaintableSize   = roomSize - DoorSize - WindowSize
		amountOfPaintNeeded = roomPaintableSize / PaintWorkQuantity

		expectedBuckets = map[PaintBucket]int{
			SmallerPaintBucket: 1,
			SmallPaintBucket:   1,
			MediumPaintBucket:  1,
		}
	)

	room, err := NewRoom([4]Wall{firstWall, secondWall, thirdWall, fourthWall})
	if err != nil {
		t.Fatal(err)
	}

	if !almostEqual(room.Size(), roomSize) {
		t.Errorf("room size is wrong. expected %.4f but got %.4f", roomSize, room.Size())
	}

	if !almostEqual(room.PaintableSize(), roomPaintableSize) {
		t.Errorf("room paintable size is wrong. expected %.4f but got %.4f", roomPaintableSize, room.PaintableSize())
	}

	if !almostEqual(room.PaintNeeded(), amountOfPaintNeeded) {
		t.Errorf("room amount of paint needed is wrong. expected %.4f but got %.4f", amountOfPaintNeeded, room.PaintNeeded())
	}

	if !reflect.DeepEqual(room.PaintBucketsNeeded(), expectedBuckets) {
		t.Errorf("paint buckets needed to paint room is wrong. expected %#v but got %#v", room.PaintBucketsNeeded(), expectedBuckets)
	}

	//

	firstWall, err = NewWall(3, 5, 1, 0)
	if err != nil {
		t.Fatalf("firstWall failed to build, err: %s", err.Error())
	}

	secondWall, err = NewWall(2.6, 2, 0, 1)
	if err != nil {
		t.Fatalf("secondWall failed to build, err: %s", err.Error())
	}

	thirdWall, err = NewWall(1.8, 2, 0, 0)
	if err != nil {
		t.Fatalf("thirdWall failed to build, err: %s", err.Error())
	}

	fourthWall, err = NewWall(5.6, 2.6, 1, 1)
	if err != nil {
		t.Fatalf("fourthWall failed to build, err: %s", err.Error())
	}

	// Expected results
	roomSize = (3 * 5) + (2.6 * 2) + (1.8 * 2) + (5.6 * 2.6)
	roomPaintableSize = roomSize - (DoorSize * 2) - (WindowSize * 2)
	amountOfPaintNeeded = roomPaintableSize / PaintWorkQuantity
	expectedBuckets = map[PaintBucket]int{
		SmallerPaintBucket: 1,
		SmallPaintBucket:   1,
		MediumPaintBucket:  1,
	}

	room, err = NewRoom([4]Wall{firstWall, secondWall, thirdWall, fourthWall})
	if err != nil {
		t.Fatal(err)
	}

	if !almostEqual(room.Size(), roomSize) {
		t.Errorf("room size is wrong. expected %.4f but got %.4f", roomSize, room.Size())
	}

	if !almostEqual(room.PaintableSize(), roomPaintableSize) {
		t.Errorf("room paintable size is wrong. expected %.4f but got %.4f", roomPaintableSize, room.PaintableSize())
	}

	if !almostEqual(room.PaintNeeded(), amountOfPaintNeeded) {
		t.Errorf("room amount of paint needed is wrong. expected %.4f but got %.4f", amountOfPaintNeeded, room.PaintNeeded())
	}

	if !reflect.DeepEqual(room.PaintBucketsNeeded(), expectedBuckets) {
		t.Errorf("paint buckets needed to paint room is wrong. expected %#v but got %#v", room.PaintBucketsNeeded(), expectedBuckets)
	}
}

// float64EqualityThreshold is the threshold used to tell if two floats are almost equal or not
// In this case, we want a precsion of 0.000000001
const float64EqualityThreshold = 1e-9

// almostEqual returns whether the two floats are almost equal
// it uses float64EqualityThreshold as the threshold
func almostEqual(a, b float64) bool {
	return math.Abs(a-b) <= float64EqualityThreshold
}
