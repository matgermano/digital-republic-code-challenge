package painter

import (
	"errors"
	"fmt"
)

// NewRoom returns a new room based of the provided walls
// It will return an error if any of the provided walls are invalid
func NewRoom(Walls [4]Wall) (room, error) {
	var outRoom room

	for i, w := range Walls {
		if err := w.Validate(); err != nil {
			return room{}, fmt.Errorf("trying to create a new room with invalid walls. wall error: %s", err.Error())
		}

		outRoom.Walls[i] = w
	}

	return outRoom, nil
}

// NewWall returns a new wall with the provided values
// It will return an error if the wall is invalid
func NewWall(height, width float64, amountOfDoors, amountOfWindows int) (Wall, error) {
	if height <= 0 || width <= 0 {
		return Wall{}, errors.New("the Wall height nor the Wall width can be equal or less than zero")
	}

	if amountOfDoors < 0 {
		return Wall{}, errors.New("the amount of doors in a Wall can not be less than zero")
	}

	if amountOfWindows < 0 {
		return Wall{}, errors.New("the amount of windows in a Wall can lot be less than zero")
	}

	outputWall := Wall{
		height:          height,
		width:           width,
		amountOfDoors:   amountOfDoors,
		amountOfWindows: amountOfWindows,
	}

	if err := outputWall.Validate(); err != nil {
		return Wall{}, err
	}

	return outputWall, nil
}
