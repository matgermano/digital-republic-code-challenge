package painter

import (
	"errors"
)

const (
	WindowHeight = 2.00                       // WindowHeight is the constant height of windows for this challenge
	WindowWidth  = 1.20                       // WindowWidth is the constant width of windows for this challenge
	WindowSize   = WindowHeight * WindowWidth // WindowSize is the constant area of windows for this challenge

	DoorHeight = 0.80                   // DoorHeight is the constant height of doors for this challenge
	DoorWidth  = 1.90                   // DoorWidth is the constant width of doors for this challenge
	DoorSize   = DoorHeight * DoorWidth // DoorSize is the constant area of doors for this challenge

	PaintWorkQuantity = 5.0 // PaintWorkQuantity is how many square meters a litre of paint can paint
)

var (
	// FlagConsiderLeftovers should be set to true if the program should consider leftovers
	FlagConsiderLeftovers bool
)

// PaintBucket represents a paint bucket
// It is limited to four sizes, and they are constant
type PaintBucket float64

// Constants representing all sizes of paintBuckets
const (
	SmallerPaintBucket PaintBucket = 0.5
	SmallPaintBucket   PaintBucket = 2.5
	MediumPaintBucket  PaintBucket = 3.6
	LargePaintBucket   PaintBucket = 18.0
)

// String() returns an human readable string specifying the bucket size
func (pb PaintBucket) String() string {
	switch pb {
	case SmallerPaintBucket:
		return "0.5L bucket"
	case SmallPaintBucket:
		return "2.5L bucket"
	case MediumPaintBucket:
		return "3.6L bucket"
	case LargePaintBucket:
		return "18.0L bucket"
	default:
		return ""
	}
}

// room represents a room in this challenge
type room struct {
	// A room will always have 4 walls
	Walls [4]Wall
}

// Size returns the total area of the room
func (r room) Size() float64 {
	var output float64

	// Iterate over all the room walls and just
	// add the wall size to the total size
	for _, w := range r.Walls {
		output += w.Size()
	}

	return output
}

// PaintableSize returns the total area of the room
// that will need to be painted
func (r room) PaintableSize() float64 {
	var output float64

	// Iterate over all the room walls and just
	// ad the wall paintable size to the total paintable size
	for _, w := range r.Walls {
		output += w.PaintableSize()
	}

	return output
}

// PaintNeeded returns the amount of paint (in liters)
// it will take to paint the room
func (r room) PaintNeeded() float64 {
	return r.PaintableSize() / PaintWorkQuantity
}

// PaintBucketsNeeded returns the amount of each bucket
// it will take to paint the whole room
func (r room) PaintBucketsNeeded() map[PaintBucket]int {
	// In this challenge, we are told that the maximum wall size is 15 sqrmt
	// Thus making the biggest room possible 60 sqrmt
	// In other words, we will never need more than 12 liter of paint
	// to paint the entire room.
	//
	// We also have a clear instruction: "always prioritize the largest buckets"
	// So, if we have a room that needs 5 liter of paint, for example,
	// we could suggest a single 18L bucket. The challange doesn't mention leftovers.
	//
	// Also, the example given in the challange: "if the user needs 19 liter of paint..."
	// is not possible because of the wall size limitation.
	//
	// All this is to say that we only need to check if the amount of paint needed
	// is less than or equal to each paint bucket size.
	// If the amount of paint needed is <= 18, for example, we suggest the 18L bucket, unless
	// it is <= 3.6, than we suggest the 3.6L bucket, and so on.
	// And since the amount of paint needed can never grow bigger than 12L, we don't ever need
	// to worry about suggesting more than 1 bucket of any size.
	//
	// Since this isn't clear whether we should consider leftovers or not, I wrote both logics.
	// If the `-leftover` flag is passed, the program will take leftovers into consideration,
	// otherwise the program will use the logic described above.

	if FlagConsiderLeftovers {
		amountOfPaintNeeded := r.PaintNeeded()

		output := make(map[PaintBucket]int)
	OUTER:
		for amountOfPaintNeeded > 0 {
			switch {
			case (amountOfPaintNeeded - float64(LargePaintBucket)) >= 0:
				amountOfPaintNeeded -= float64(LargePaintBucket)
				output[LargePaintBucket]++
				continue OUTER
			case (amountOfPaintNeeded - float64(MediumPaintBucket)) >= 0:
				amountOfPaintNeeded -= float64(MediumPaintBucket)
				output[MediumPaintBucket]++
				continue OUTER
			case (amountOfPaintNeeded - float64(SmallPaintBucket)) >= 0:
				amountOfPaintNeeded -= float64(SmallPaintBucket)
				output[SmallPaintBucket]++
				continue OUTER
			case (amountOfPaintNeeded - float64(SmallerPaintBucket)) >= 0:
				amountOfPaintNeeded -= float64(SmallerPaintBucket)
				output[SmallerPaintBucket]++
				continue OUTER
			default:
				// If we got here, it means we need less than 0.5L of paint
				// so we just add one more SmallerPaintBucket and break the loop
				output[SmallerPaintBucket]++
				break OUTER
			}
		}

		return output
	}

	amountOfPaintNeeded := r.PaintNeeded()

	switch {
	case amountOfPaintNeeded <= float64(SmallerPaintBucket):
		return map[PaintBucket]int{
			SmallerPaintBucket: 1,
		}
	case amountOfPaintNeeded <= float64(SmallPaintBucket):
		return map[PaintBucket]int{
			SmallPaintBucket: 1,
		}
	case amountOfPaintNeeded <= float64(MediumPaintBucket):
		return map[PaintBucket]int{
			MediumPaintBucket: 1,
		}
	case amountOfPaintNeeded <= float64(LargePaintBucket):
		return map[PaintBucket]int{
			LargePaintBucket: 1,
		}
	}

	return nil
}

// Wall represents a room Wall
type Wall struct {
	height          float64 // height represents the height of the wall
	width           float64 // width represents the width of the wall
	amountOfDoors   int     // amountOfDoors represents the amount of doors present in this wall
	amountOfWindows int     // amountOfWindows represents the amount of windows present in this wall
}

// Size will return the total area of the wall
func (w Wall) Size() float64 {
	return w.height * w.width
}

// Validate validates the wall
// If the wall is not valid, it will return an error
func (w Wall) Validate() error {
	WallSize := w.Size()

	// Get the total area of windows and doors combined
	totalDoorsAndWindowsSize := (float64(w.amountOfWindows) * WindowSize) + (float64(w.amountOfDoors) * DoorSize)

	switch {
	case WallSize < 1, WallSize > 15:
		// If the wall size is smaller than 1 sqrmt or greater than 15 sqrmt,
		// it means this wall is invalid for this challenge
		return errors.New("wall size can't be smaller than 1 sqrmt nor bigger than 15 sqrmt")
	case totalDoorsAndWindowsSize > (WallSize / 2):
		// If the combined size of the doors and the windows are greater than 50% of the total area of the wall,
		// it means this wall is invalid for this challenge
		return errors.New("total area used by windows and doors can't be bigger than 50% of the total size of the wall")
	}

	if w.amountOfDoors > 0 {
		if w.height < 2.20 {
			// If we have a door on this wall, it must be taller than 2.2 meters
			// otherwise it is invalid for this challenge
			return errors.New("a wall with a door has to be at least 2.2 meters tall")
		}
	}

	return nil
}

// PaintableSize returns the total area of this wall
// that will need to be painted
func (w Wall) PaintableSize() float64 {
	outputSize := w.Size()

	// If this wall has no doors nor windows, the whole area
	// will need to be painted
	if w.amountOfDoors == 0 && w.amountOfWindows == 0 {
		return outputSize
	}

	if w.amountOfWindows > 0 {
		// This wall have window(s), so take out the related area
		windowsTotalSize := float64(w.amountOfWindows) * WindowSize
		outputSize -= windowsTotalSize
	}

	if w.amountOfDoors > 0 {
		// This wall have door(s), so take out the related area
		doorsTotalSize := float64(w.amountOfDoors) * DoorSize
		outputSize -= doorsTotalSize
	}

	return outputSize
}
