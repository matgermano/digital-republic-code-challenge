package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"strconv"
	"strings"

	"gitlab.com/matgermano/digital-republic-code-challenge/painter"
)

func init() {
	flag.BoolVar(&painter.FlagConsiderLeftovers, "leftover", false, "Set the program to consider leftovers")
}

func main() {
	// Parse all flags
	flag.Parse()

	reader := bufio.NewReader(os.Stdin)

	firstWall, err := parseWallFromReader(reader, "first")
	if err != nil {
		panic(err)
	}

	secondWall, err := parseWallFromReader(reader, "second")
	if err != nil {
		panic(err)
	}

	thirdWall, err := parseWallFromReader(reader, "third")
	if err != nil {
		panic(err)
	}

	frouthWall, err := parseWallFromReader(reader, "fourth")
	if err != nil {
		panic(err)
	}

	room, err := painter.NewRoom([4]painter.Wall{firstWall, secondWall, thirdWall, frouthWall})
	if err != nil {
		panic(err)
	}

	out := fmt.Sprintf("\n\nYour room has %.4f sqrmt in total\nIt has %.4f sqrmt of paintable space\nIt will need %.4f liters of paint\n", room.Size(), room.PaintableSize(), room.PaintNeeded())
	out += prettifyOutput(room.PaintBucketsNeeded())

	fmt.Println(out)
}

// parseWallFromReader parses the input from the reader into a wall
func parseWallFromReader(reader *bufio.Reader, name string) (painter.Wall, error) {
	var height, width float64
	var amountOfDoors, amountOfWindows int

	fmt.Printf("Enter %s wall height\n", name)
	text, err := reader.ReadString('\n')
	if err != nil {
		return painter.Wall{}, err
	}
	text = strings.TrimSuffix(text, "\r\n")

	height, err = strconv.ParseFloat(text, 64)
	if err != nil {
		return painter.Wall{}, err
	}

	fmt.Printf("Enter %s wall width\n", name)
	text, err = reader.ReadString('\n')
	if err != nil {
		return painter.Wall{}, err
	}
	text = strings.TrimSuffix(text, "\r\n")

	width, err = strconv.ParseFloat(text, 64)
	if err != nil {
		return painter.Wall{}, err
	}

	fmt.Printf("Enter amount of windows on the %s wall\n", name)
	text, err = reader.ReadString('\n')
	if err != nil {
		return painter.Wall{}, err
	}
	text = strings.TrimSuffix(text, "\r\n")

	amountOfWindows, err = strconv.Atoi(text)
	if err != nil {
		return painter.Wall{}, err
	}

	fmt.Printf("Enter amount of doors on the %s wall\n", name)
	text, err = reader.ReadString('\n')
	if err != nil {
		return painter.Wall{}, err
	}
	text = strings.TrimSuffix(text, "\r\n")

	amountOfDoors, err = strconv.Atoi(text)
	if err != nil {
		return painter.Wall{}, err
	}

	wall, err := painter.NewWall(height, width, amountOfDoors, amountOfWindows)
	if err != nil {
		return painter.Wall{}, err
	}

	return wall, nil
}

// prettifyOutput gets the data from a map[paintBucket]int
// and prints it out nicely in a human readable form
func prettifyOutput(in map[painter.PaintBucket]int) string {
	builder := strings.Builder{}

	var i int

	for pb, amount := range in {
		if amount > 0 {
			var s string
			if amount > 1 {
				s = "s"
			}

			var first string

			if i == 0 {
				first = "You will need"
				i++
			} else {
				first = "and"
			}

			builder.WriteString(fmt.Sprintf("%s %d %s%s ", first, amount, pb, s))
		}
	}

	return builder.String()
}
